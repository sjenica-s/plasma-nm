# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the plasma-nm package.
#
# Sveinn í Felli <sv1@fellsnet.is>, 2022.
msgid ""
msgstr ""
"Project-Id-Version: plasma-nm\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2021-08-28 00:19+0000\n"
"PO-Revision-Date: 2022-08-23 08:23+0000\n"
"Last-Translator: Sveinn í Felli <sv1@fellsnet.is>\n"
"Language-Team: Icelandic\n"
"Language: is\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 21.12.3\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Sveinn í Felli"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "sv1@fellsnet.is"

#: package/contents/ui/ConnectDialog.qml:136
#, kde-format
msgid "Invalid input."
msgstr "Ógilt ílag."

#: package/contents/ui/ConnectionItemDelegate.qml:107
#, kde-format
msgid "Connect to"
msgstr "Tengjast við"

#: package/contents/ui/main.qml:82
#, kde-format
msgid "Wi-Fi is disabled"
msgstr "Wi-Fi er óvirkt"

#: package/contents/ui/main.qml:86
#, kde-format
msgid "Enable"
msgstr "Virkja"

#: package/contents/ui/main.qml:96
#, kde-format
msgid "Disable Wi-Fi"
msgstr "Gera Wi-Fi óvirkt"

#: package/contents/ui/main.qml:102
#, kde-format
msgid "Add Custom Connection"
msgstr "Bæta við sérsniðinni tengingu"

#: package/contents/ui/main.qml:108
#, kde-format
msgid "Show Saved Connections"
msgstr "Sýna vistaðar tengingar"

#: package/contents/ui/NetworkSettings.qml:14
#, kde-format
msgid "Add new Connection"
msgstr "Bæta við nýrri tengingu"

#: package/contents/ui/NetworkSettings.qml:34
#, kde-format
msgid "Save"
msgstr "Vista"

#: package/contents/ui/NetworkSettings.qml:44
#, kde-format
msgid "General"
msgstr "Almennt"

#: package/contents/ui/NetworkSettings.qml:49
#, kde-format
msgid "SSID:"
msgstr "SSID:"

#: package/contents/ui/NetworkSettings.qml:58
#, kde-format
msgid "Hidden Network:"
msgstr "Falið Wi-Fi netkerfi:"

#: package/contents/ui/NetworkSettings.qml:64
#, kde-format
msgid "Security"
msgstr "Öryggi"

#: package/contents/ui/NetworkSettings.qml:70
#, kde-format
msgid "Security type:"
msgstr "Tegund öryggis:"

#: package/contents/ui/NetworkSettings.qml:79
#, kde-format
msgid "None"
msgstr "Ekkert"

#: package/contents/ui/NetworkSettings.qml:80
#, kde-format
msgid "WEP Key"
msgstr "WEP-lykill"

#: package/contents/ui/NetworkSettings.qml:81
#, kde-format
msgid "Dynamic WEP"
msgstr "Breytilegt WEP"

#: package/contents/ui/NetworkSettings.qml:82
#, kde-format
msgid "WPA/WPA2 Personal"
msgstr "WPA/WPA2 einka"

#: package/contents/ui/NetworkSettings.qml:83
#, kde-format
msgid "WPA/WPA2 Enterprise"
msgstr "WPA/WPA2 fyrirtækja"

#: package/contents/ui/NetworkSettings.qml:108
#, kde-format
msgid "Password:"
msgstr "Lykilorð:"

#: package/contents/ui/NetworkSettings.qml:116
#, kde-format
msgid "Authentication:"
msgstr "Auðkenning:"

#: package/contents/ui/NetworkSettings.qml:119
#, kde-format
msgid "TLS"
msgstr "TLS"

#: package/contents/ui/NetworkSettings.qml:119
#, kde-format
msgid "LEAP"
msgstr "LEAP"

#: package/contents/ui/NetworkSettings.qml:119
#, kde-format
msgid "FAST"
msgstr "FAST"

#: package/contents/ui/NetworkSettings.qml:120
#, kde-format
msgid "Tunneled TLS"
msgstr "Tunneled TLS"

#: package/contents/ui/NetworkSettings.qml:121
#, kde-format
msgid "Protected EAP"
msgstr "Varið EAP"

#: package/contents/ui/NetworkSettings.qml:130
#, kde-format
msgid "IP settings"
msgstr "Stillingar IP-vistfangs"

#: package/contents/ui/NetworkSettings.qml:136
#, kde-format
msgid "Automatic"
msgstr "Sjálfvirkt"

#: package/contents/ui/NetworkSettings.qml:136
#, kde-format
msgid "Manual"
msgstr "Handvirkt"

#: package/contents/ui/NetworkSettings.qml:146
#, kde-format
msgid "IP Address:"
msgstr "IP-vistfang:"

#: package/contents/ui/NetworkSettings.qml:158
#, kde-format
msgid "Gateway:"
msgstr "Netgátt"

#: package/contents/ui/NetworkSettings.qml:170
#, kde-format
msgid "Network prefix length:"
msgstr "Lengd forskeytis netkerfis:"

#: package/contents/ui/NetworkSettings.qml:183
#, kde-format
msgid "DNS:"
msgstr "DNS: "

#: package/contents/ui/PasswordField.qml:13
#, kde-format
msgid "Password…"
msgstr "Lykilorð…"

#: wifisettings.cpp:32
#, kde-format
msgid "Wi-Fi networks"
msgstr "Wi-Fi netkerfi"

#: wifisettings.cpp:33
#, kde-format
msgid "Martin Kacej"
msgstr "Martin Kacej"
