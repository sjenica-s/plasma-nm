# Lithuanian translations for plasma-nm package.
# Copyright (C) 2019 This file is copyright:
# This file is distributed under the same license as the plasma-nm package.
# Automatically generated, 2019.
#
msgid ""
msgstr ""
"Project-Id-Version: plasma-nm\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2021-08-28 00:19+0000\n"
"PO-Revision-Date: 2021-08-21 12:30+0300\n"
"Last-Translator: Moo\n"
"Language-Team: lt\n"
"Language: lt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n==1 ? 0 : n%10>=2 && (n%100<10 || n"
"%100>=20) ? 1 : n%10==0 || (n%100>10 && n%100<20) ? 2 : 3);\n"
"X-Generator: Poedit 3.0\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Moo"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "<>"

#: package/contents/ui/ConnectDialog.qml:136
#, kde-format
msgid "Invalid input."
msgstr "Neteisinga įvestis."

#: package/contents/ui/ConnectionItemDelegate.qml:107
#, kde-format
msgid "Connect to"
msgstr "Prisijungti prie"

#: package/contents/ui/main.qml:82
#, kde-format
msgid "Wi-Fi is disabled"
msgstr "Belaidis (Wi-Fi) išjungtas"

#: package/contents/ui/main.qml:86
#, kde-format
msgid "Enable"
msgstr "Įjungti"

#: package/contents/ui/main.qml:96
#, kde-format
msgid "Disable Wi-Fi"
msgstr "Išjungti belaidį (Wi-Fi)"

#: package/contents/ui/main.qml:102
#, kde-format
msgid "Add Custom Connection"
msgstr "Pridėti tinkintą ryšį"

#: package/contents/ui/main.qml:108
#, kde-format
msgid "Show Saved Connections"
msgstr "Rodyti įrašytus ryšius"

#: package/contents/ui/NetworkSettings.qml:14
#, kde-format
msgid "Add new Connection"
msgstr "Pridėti naują ryšį"

#: package/contents/ui/NetworkSettings.qml:34
#, kde-format
msgid "Save"
msgstr "Įrašyti"

#: package/contents/ui/NetworkSettings.qml:44
#, kde-format
msgid "General"
msgstr "Bendra"

#: package/contents/ui/NetworkSettings.qml:49
#, kde-format
msgid "SSID:"
msgstr "SSID:"

#: package/contents/ui/NetworkSettings.qml:58
#, kde-format
msgid "Hidden Network:"
msgstr "Paslėptas tinklas:"

#: package/contents/ui/NetworkSettings.qml:64
#, kde-format
msgid "Security"
msgstr "Saugumas"

#: package/contents/ui/NetworkSettings.qml:70
#, kde-format
msgid "Security type:"
msgstr "Saugumo tipas:"

#: package/contents/ui/NetworkSettings.qml:79
#, kde-format
msgid "None"
msgstr "Nėra"

#: package/contents/ui/NetworkSettings.qml:80
#, kde-format
msgid "WEP Key"
msgstr "WEP raktas"

#: package/contents/ui/NetworkSettings.qml:81
#, kde-format
msgid "Dynamic WEP"
msgstr "Dinaminis WEP"

#: package/contents/ui/NetworkSettings.qml:82
#, kde-format
msgid "WPA/WPA2 Personal"
msgstr "WPA/WPA2 asmeninis"

#: package/contents/ui/NetworkSettings.qml:83
#, kde-format
msgid "WPA/WPA2 Enterprise"
msgstr "WPA/WPA2 verslo"

#: package/contents/ui/NetworkSettings.qml:108
#, kde-format
msgid "Password:"
msgstr "Slaptažodis:"

#: package/contents/ui/NetworkSettings.qml:116
#, kde-format
msgid "Authentication:"
msgstr "Tapatybės nustatymas:"

#: package/contents/ui/NetworkSettings.qml:119
#, kde-format
msgid "TLS"
msgstr "TLS"

#: package/contents/ui/NetworkSettings.qml:119
#, kde-format
msgid "LEAP"
msgstr "LEAP"

#: package/contents/ui/NetworkSettings.qml:119
#, kde-format
msgid "FAST"
msgstr "FAST"

#: package/contents/ui/NetworkSettings.qml:120
#, kde-format
msgid "Tunneled TLS"
msgstr "Tuneliuotas TLS"

#: package/contents/ui/NetworkSettings.qml:121
#, kde-format
msgid "Protected EAP"
msgstr "Apsaugotas EAP"

#: package/contents/ui/NetworkSettings.qml:130
#, kde-format
msgid "IP settings"
msgstr "IP nuostatos"

#: package/contents/ui/NetworkSettings.qml:136
#, kde-format
msgid "Automatic"
msgstr "Automatiškai"

#: package/contents/ui/NetworkSettings.qml:136
#, kde-format
msgid "Manual"
msgstr "Rankiniu būdu"

#: package/contents/ui/NetworkSettings.qml:146
#, kde-format
msgid "IP Address:"
msgstr "IP adresas:"

#: package/contents/ui/NetworkSettings.qml:158
#, kde-format
msgid "Gateway:"
msgstr "Tinklų sietuvas:"

#: package/contents/ui/NetworkSettings.qml:170
#, kde-format
msgid "Network prefix length:"
msgstr "Tinklo priešdėlio ilgis:"

#: package/contents/ui/NetworkSettings.qml:183
#, kde-format
msgid "DNS:"
msgstr "DNS:"

#: package/contents/ui/PasswordField.qml:13
#, kde-format
msgid "Password…"
msgstr "Slaptažodis…"

#: wifisettings.cpp:32
#, kde-format
msgid "Wi-Fi networks"
msgstr "Belaidžiai (Wi-Fi) tinklai"

#: wifisettings.cpp:33
#, kde-format
msgid "Martin Kacej"
msgstr "Martin Kacej"

#~ msgctxt "@action:button"
#~ msgid "Cancel"
#~ msgstr "Atsisakyti"

#~ msgctxt "@action:button"
#~ msgid "Done"
#~ msgstr "Atlikta"

#~ msgid "Password..."
#~ msgstr "Slaptažodis..."

#~ msgid "Create Hotspot"
#~ msgstr "Sukurti viešosios prieigos tašką"

#~ msgid "Wi-Fi Hotspot"
#~ msgstr "Belaidis (Wi-Fi) viešosios prieigos taškas"

#~ msgid "Configure"
#~ msgstr "Konfigūruoti"

#~ msgid "SSID"
#~ msgstr "SSID"

#~ msgid "My Hotspot"
#~ msgstr "Mano viešosios prieigos taškas"

#~ msgid "Hide this network"
#~ msgstr "Slėpti šį tinklą"

#~ msgid "Protect hotspot with WPA2/PSK password"
#~ msgstr "Apsaugoti viešosios prieigos tašką WPA2/PSK slaptažodžiu"

#~ msgid "Save Hotspot configuration"
#~ msgstr "Įrašyti viešosios prieigos taško konfigūraciją"

#~ msgid "Disable Wi-Fi Hotspot"
#~ msgstr "Išjungti belaidį (Wi-Fi) prieigos tašką"

#~ msgid "Enable Wi-Fi Hotspot"
#~ msgstr "Įjungti belaidį (Wi-Fi) prieigos tašką"

#~ msgid "Hotspot is inactive"
#~ msgstr "Viešosios prieigos taškas yra pasyvus"

#~ msgid "Not possible to start Access point."
#~ msgstr "Neįmanoma paleisti prieigos taško."

#~ msgid "Access point running: %1"
#~ msgstr "Veikiantis prieigos taškas: %1"

#~ msgid "No suitable configuration found."
#~ msgstr "Nerasta jokios tinkamos konfigūracijos."

#~ msgid "Access point available: %1"
#~ msgstr "Prieinamas prieigos taškas: %1"

#~ msgid "Connection Name"
#~ msgstr "Ryšio pavadinimas"

#~ msgid "(Unchanged)"
#~ msgstr "(Nepakeistas)"

#~ msgid "Delete connection %1 from device?"
#~ msgstr "Ištrinti iš įrenginio ryšį %1?"

#~ msgid "Delete"
#~ msgstr "Ištrinti"

#~ msgid "Saved networks"
#~ msgstr "Įrašyti tinklai"

#~ msgid "Available networks"
#~ msgstr "Prieinami tinklai"

#~ msgid "DNS"
#~ msgstr "DNS"

#~ msgid "Wi-fi"
#~ msgstr "Belaidis (Wi-fi)"
