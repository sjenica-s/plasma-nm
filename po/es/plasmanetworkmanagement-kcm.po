# Spanish translations for plasmanetworkmanagement-kcm.po package.
# Copyright (C) 2017 This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Automatically generated, 2017.
# Eloy <ecuadra@eloihr.net>, 2017.
# Eloy Cuadra <ecuadra@eloihr.net>, 2017, 2019, 2020, 2021.
msgid ""
msgstr ""
"Project-Id-Version: plasmanetworkmanagement-kcm\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-11-16 00:48+0000\n"
"PO-Revision-Date: 2021-05-31 23:59+0200\n"
"Last-Translator: Eloy Cuadra <ecuadra@eloihr.net>\n"
"Language-Team: Spanish <kde-l10n-es@kde.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 21.04.1\n"

#: kcm.cpp:344
#, kde-format
msgid "my_shared_connection"
msgstr "mi_conexión_compartida"

#: kcm.cpp:415
#, kde-format
msgid "Export VPN Connection"
msgstr "Exportar conexión VPN"

#: kcm.cpp:440
#, kde-format
msgid "Do you want to save changes made to the connection '%1'?"
msgstr "¿Desea guardar los cambios realizados en la conexión «%1»?"

#: kcm.cpp:441
#, kde-format
msgctxt "@title:window"
msgid "Save Changes"
msgstr "Guardar cambios"

#: kcm.cpp:529
#, kde-format
msgid "Import VPN Connection"
msgstr "Importar conexión VPN"

#: qml/AddConnectionDialog.qml:15
msgctxt "@title:window"
msgid "Choose a Connection Type"
msgstr "Escoja un tipo de conexión"

#: qml/AddConnectionDialog.qml:159
msgid "Create"
msgstr "Crear"

#: qml/AddConnectionDialog.qml:169 qml/ConfigurationDialog.qml:112
msgid "Cancel"
msgstr "Cancelar"

#: qml/AddConnectionDialog.qml:186 qml/main.qml:193
msgid "Configuration"
msgstr "Configuración"

#: qml/ConfigurationDialog.qml:16
msgctxt "@title:window"
msgid "Configuration"
msgstr "Configuración"

#: qml/ConfigurationDialog.qml:37
msgid "General"
msgstr "General"

#: qml/ConfigurationDialog.qml:42
msgid "Ask for PIN on modem detection"
msgstr "Solicitar el PIN al detectar un módem"

#: qml/ConfigurationDialog.qml:49
msgid "Show virtual connections"
msgstr "Mostrar conexiones virtuales"

#: qml/ConfigurationDialog.qml:57
msgid "Hotspot"
msgstr "Punto de acceso"

#: qml/ConfigurationDialog.qml:63
msgid "Hotspot name:"
msgstr "Nombre del punto de acceso:"

#: qml/ConfigurationDialog.qml:73
msgid "Hotspot password:"
msgstr "Contraseña del punto de acceso:"

#: qml/ConfigurationDialog.qml:103
msgid "Ok"
msgstr "Aceptar"

#: qml/ConnectionItem.qml:98
msgid "Connect"
msgstr "Conectar"

#: qml/ConnectionItem.qml:98
msgid "Disconnect"
msgstr "Desconectar"

#: qml/ConnectionItem.qml:111
msgid "Delete"
msgstr "Borrar"

#: qml/ConnectionItem.qml:121
msgid "Export"
msgstr "Exportar"

#: qml/ConnectionItem.qml:146
msgid "Connected"
msgstr "Conectado"

#: qml/ConnectionItem.qml:148
msgid "Connecting"
msgstr "Conectando"

#: qml/main.qml:139
msgid "Add new connection"
msgstr "Añadir nueva conexión"

#: qml/main.qml:153
msgid "Remove selected connection"
msgstr "Eliminar la conexión seleccionada"

#: qml/main.qml:169
msgid "Export selected connection"
msgstr "Exportar la conexión seleccionada"

#: qml/main.qml:221
msgctxt "@title:window"
msgid "Remove Connection"
msgstr "Eliminar conexión"

#: qml/main.qml:222
msgid "Do you want to remove the connection '%1'?"
msgstr "¿Desea eliminar la conexión «%1»?"

#~ msgid "Search…"
#~ msgstr "Buscar..."

#~ msgid "Type here to search connections..."
#~ msgstr "Escriba aquí para buscar conexiones..."

#~ msgid "Close"
#~ msgstr "Cerrar"
